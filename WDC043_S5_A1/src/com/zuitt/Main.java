package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {



//        person1
        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.setNumbers("+639394567890");
        contact1.setNumbers("+639393214578");
        contact1.setAddresses("My home is in Quezon city");
        contact1.setAddresses("My office is in Makati city");

//        person2
        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        contact2.setNumbers("+63924567890");
        contact2.setNumbers("+63924865338");
        contact2.setAddresses("My home is in Caloocan City");
        contact2.setAddresses("My office is in Pasay City");


        Phonebook phonebook = new Phonebook();
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.printPhonebook();


    }
}
