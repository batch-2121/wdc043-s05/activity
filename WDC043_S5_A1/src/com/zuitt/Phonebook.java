package com.zuitt;

import java.util.ArrayList;

public class Phonebook{

    public ArrayList<Contact> contacts = new ArrayList<>();

//    constructors
    public Phonebook() {}

    public Phonebook(Contact contact) {


        this.contacts.add(contact);
    }

//    Getters and Setters

    //GET
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    //SET
    public void setContacts(Contact contacts){
        this.contacts.add(contacts);
    }

    //methods
    public void printPhonebook(){

        for (int j=0; j<contacts.size();j++){

            if (contacts.size() == 0) {
                System.out.println("Empty Phonebook");
            } else {
                System.out.println(contacts.get(j).name);
                System.out.println("----------------------------");
                System.out.println(contacts.get(j).name + " has the following registered numbers:");
                for (int i =0; i<contacts.size();i++){
                    System.out.println(contacts.get(j).numbers.get(i));
                }
                System.out.println("--------------------------------");
                System.out.println(contacts.get(j).name+" has the following registered addresses:");
                for (int i =0; i<contacts.size();i++){
                    System.out.println(contacts.get(j).addresses.get(i));
                }
                System.out.println("=============================================");
            }
        }


    }
}
