package com.zuitt;

import java.util.ArrayList;

public class Contact {

    public String name;
    public ArrayList<String> numbers = new ArrayList<>();
    public ArrayList<String> addresses = new ArrayList<>();


// constructors
    public Contact (){}

    public Contact (String name, String numbers, String addresses){

        this.name = name;
        this.numbers.add(numbers);
        this.addresses.add(addresses);
    }

//    Getters and setters

    //GETTERS
    public String getName(){
        return this.name;
    }

    public ArrayList<String> getNumber(){
        return this.numbers;
    }

    public ArrayList<String> getAddresses(){
        return this.addresses;
    }

    //SETTERS
    public void setName(String name){
        this.name = name;
    }

    public void setNumbers(String numbers){
        this.numbers.add(numbers);
    }

    public void setAddresses(String addresses){
        this.addresses.add(addresses);
    }




}
